package tfs.naval.ship

import tfs.naval.model.Ship

trait ShipController {

  // определить, подходит ли корабль по своим характеристикам
  def validateShip(ship: Ship): Boolean
}
