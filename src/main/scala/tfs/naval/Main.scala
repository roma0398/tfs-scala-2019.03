package tfs.naval

import tfs.naval.field.MockFieldController
import tfs.naval.fleet.MockFleetController
import tfs.naval.io.FixedShipsReader
import tfs.naval.model.Ship
import tfs.naval.ship.MockShipController

object Main extends App {

  val shipsReader = new FixedShipsReader

  val shipController = new MockShipController
  val fleetController = new MockFleetController
  val fieldController = new MockFieldController

  val solution  = new Solution(shipController, fieldController, fleetController)

  val initField = Vector.fill(10){
    Vector.fill(10)(false)
  }

  val result = shipsReader.read.foldLeft(initField -> Map.empty[String, Ship]) {
    case (state, (name, ship: Ship)) => solution.tryAddShip(state, name, ship)
  }

  result._2.keys.foreach(println)

}
